Developer Module that assists with image manipulation 
process in varies modes. 
Like the below types of barcode are generating by this module.

Grayscale
Brightness
Shading
Negative
Pixelation
Crop
Resize
Rotate
Watermark Text
Watermark Image
Comic
Merge Images
Flip Display
Create animated gif image

Coder Sniffer
-------------

See the README.txt file in the coder_sniffer directory.


Installation
------------

Copy coder.module to your module directory and then enable
on the admin modules page.
Enable the modules and the "image_manipulation" menu callback
will create to do the test mode.


Automated Testing (PHPUnit)
---------------------------

Coder Sniffer comes with a PHPUnit test suite to make sure
the sniffs work correctly.
Use Composer to install the dependencies:

  composer install

Then execute the tests:

  ./vendor/bin/phpunit


Author
------
Ramasubbu
m.n.ramasubbu@gmail.com
